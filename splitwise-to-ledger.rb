#!/usr/bin/env ruby

# Fetch all your expenses from
#
#  https://secure.splitwise.com/api/v3.0/get_expenses?limit=0
#
# Pipe through this utility
#
#   ruby ./splitwise-to-ledger.rb $YOUR_ID < get_expenses.json > splitwise.ledger
#
# Confirm balance
#
#  ledger -f splitwise.ledger balance 'Your Name'
#
# Print your transactions
#
#  ledger -f splitwise.ledger register 'Your Name'
#
# Optionally:
#
# Fetch all your groups from
#
#  https://secure.splitwise.com/api/v3.0/get_groups
#
# Supply to utility
#
#   ruby ./splitwise-to-ledger.rb $YOUR_ID get_groups.json < get_expenses.json > splitwise.ledger
#

require 'erb'
require 'json'
require 'date'

TEMPLATE = ERB.new(DATA.read, 0, '-')

Transaction = Struct.new(:user_id, :name, :value)

def user_name(user)
  user.values_at('first_name', 'last_name').compact.join(' ')
end

def format_currency(code, value)
  case
  when code == 'JPY' && value.modulo(1).zero?
    sprintf('% 8.0f %s', value, code)
  else
    sprintf('% 8.2f %s', value, code)
  end
end

def extract_part(users, part_name, coeff)
  part_name = part_name.to_s
  users.select { |u| u[part_name].to_f != 0 }.map do |u|
    user = u['user']
    Transaction.new(
      user['id'],
      user_name(user),
      u[part_name].to_f * coeff,
    )
  end
end

def format_group_expense(expense, group_names)
  description, date, currency_code, group_id =
    expense.values_at('description', 'date', 'currency_code', 'group_id')

  date = Date.parse(date)

  paid = extract_part(expense['users'], 'paid_share',  1.0)
  owed = extract_part(expense['users'], 'owed_share', -1.0)

  group_name = (group_names[group_id] if group_names) || group_id.to_s

  TEMPLATE.result(binding)
end

def format_individual_expense(expense, self_id)
  description, date, currency_code, repayments =
    expense.values_at('description', 'date', 'currency_code', 'repayments')

  date = Date.parse(date)

  group_name = nil

  users_by_id = expense['users'].each_with_object({}) do |user, index|
    index[user['user_id']] = user['user']
  end

  if (repayment = repayments.detect { |r| r['to'] == self_id })
    payer = repayment['to']
    ower  = repayment['from']
  elsif (repayment = repayments.detect { |r| r['from'] == self_id })
    payer = repayment['to']
    ower  = repayment['from']
  else
    raise 'Not involved in individual expense'
  end

  amount = repayment['amount'].to_f

  paid = [Transaction.new(payer, user_name(users_by_id[payer]), amount)]
  owed = [Transaction.new(ower,  user_name(users_by_id[ower]),  amount * -1.0)]

  TEMPLATE.result(binding)
end

def format_expense(expense, group_names, self_id)
  if expense['group_id'].nil?
    format_individual_expense(expense, self_id)
  else
    format_group_expense(expense, group_names)
  end
end

def transform(expenses:, groups: nil, self_id: nil)
  group_names =
    if groups
      groups['groups']
        .each_with_object({}) { |group, index| index[group['id']] = group['name'] }
    else
      {}
    end

  expenses['expenses']
    .reject { |x| x['deleted_at'] }
    .sort_by { |x| x['date'] }
    .each { |x| puts format_expense(x, group_names, self_id) }
end

def main(self_id, groups_filename = nil)
  expenses = JSON.parse(STDIN.read)
  groups = (JSON.parse(File.read(groups_filename)) if groups_filename)
  transform(expenses: expenses, groups: groups, self_id: Integer(self_id))
end

main(*ARGV)

__END__
<%= date %> <%= description %>
<% (paid.concat(owed)).each do |tx| -%>
	<%=
      sprintf('%-30s', (group_name || 'Individual') + ':' + tx.name)
    %>  <%= format_currency(currency_code, tx.value) %>
<% end %>
